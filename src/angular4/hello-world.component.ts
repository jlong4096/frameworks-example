/**
 * Created by jlong on 5/2/17.
 */
import 'reflect-metadata';
import {Component} from "@angular/core";

@Component({
    selector: 'simpleDemo',
    template: '<h1>Hello World, Angular 4</h1>'
})
export class MyHelloWorldClass {}