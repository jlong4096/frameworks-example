/**
 * Created by jlong on 5/9/17.
 */

//Angular4
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {PluginModule as Angular4Module} from './angular4/hello-world.module.ts';

//React
import {Render as ReactRender} from './react/hello-world-container.js';

class Ctrl {
    constructor($scope) {
        this.$scope = $scope;

        platformBrowserDynamic().bootstrapModule(Angular4Module);
        ReactRender();
    }
}

Ctrl.$inject = ['$scope'];

export default Ctrl;