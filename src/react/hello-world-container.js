/**
 * Created by jlong on 4/26/17.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import {HelloWorld} from './hello-world.jsx';

export function Render() {
    let container = document.getElementById('react-unique-id');
    let component = ReactDOM.render(React.createElement(HelloWorld), container);
}