/**
 * Created by jlong on 4/26/17.
 */
import React from 'react';
export class HelloWorld extends React.Component {
    render() {
        return <h1>Hello World, React</h1>;
    }
}